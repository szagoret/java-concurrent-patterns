# Advanced Java Concurrent Patterns #

Concurrency is a hard to master topic. 
This Java course takes you from the basics you already know, (runnable and synchronization) to the next level: the java.util.concurrent API.