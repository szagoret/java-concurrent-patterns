package com.java.concurrency.executors;

import java.util.concurrent.*;

/**
 * Created by szagoret on 18.07.2017.
 */
public class PlayingWithCallablesAndFutures {
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        Callable<String> task = () -> {
            throw new IllegalStateException("I throw an exception in thread " + Thread.currentThread().getName());
        };

        ExecutorService executor = Executors.newFixedThreadPool(4);

        try {
            for (int i = 0; i < 10; i++) {
                Future<String> future = executor.submit(task);
                System.out.println("I get: " + future.get());
            }
        } finally {
            executor.shutdown();
        }
    }
}
