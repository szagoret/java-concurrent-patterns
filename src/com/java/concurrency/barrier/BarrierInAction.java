package com.java.concurrency.barrier;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by szagoret on 19.07.2017.
 */
public class BarrierInAction {
    public static void main(String[] args) {


        class Fiend implements Callable<String> {
            private CyclicBarrier barrier;

            public Fiend(CyclicBarrier barrier) {
                this.barrier = barrier;
            }

            @Override
            public String call() throws Exception {
                try {
                    Random random = new Random();
                    Thread.sleep(random.nextInt(20) * 100 + 100);
                    System.out.println("I just arrived, waiting for the others...");

                    barrier.await(5, TimeUnit.SECONDS);

                    System.out.println("Let's go to the cinema!");
                    return "ok";
                } catch (InterruptedException e) {
                    System.out.println("Interrupted");
                }
                return "nok";
            }
        }

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        CyclicBarrier barrier = new CyclicBarrier(4, () -> System.out.println("Barrier opening"));
        List<Future<String>> futures = new ArrayList<>();

        try {
//            IntStream.range(0, 4).forEach(n -> futures.add(executorService.submit(new Fiend(barrier))));
            for (int i = 0; i < 3; i++) {
                Fiend fiend = new Fiend(barrier);
                futures.add(executorService.submit(fiend));
            }

            futures.forEach(future -> {
                try {
                    future.get(200, TimeUnit.MILLISECONDS);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    System.out.println("Time out");
                    future.cancel(true);
                }
            });
        } finally {
            executorService.shutdown();
        }
    }
}
